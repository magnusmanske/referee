'use strict';

let router ;
let app ;
let wd = new WikiData() ;

$(document).ready ( function () {
    vue_components.toolname = 'tabernacle' ;
    vue_components.components_base_url = 'https://tools.wmflabs.org/magnustools/resources/vue/' ; // For testing; turn off to use tools-static
    Promise.all ( [
            vue_components.loadComponents ( ['widar','wd-date','wd-link','tool-translate','tool-navbar','commons-thumbnail','snak','value-validator','typeahead-search',
                'vue_components/snak-editor.html',
                'vue_components/main-page.html',
                'vue_components/label-editor.html',
                'vue_components/claim-references-qualifiers.html',
                'vue_components/batch-navigator.html',
                'vue_components/tabernacle-cell.html',
                'vue_components/table-page.html',
                ] ) ,
            new Promise(function(resolve, reject) {
                resolve() ;
            } )
    ] ) .then ( () => {
        wd_link_wd = wd ;

        const routes = [
            { path: '/', component: MainPage , props:true },
            { path: '/tab', component: TablePage , props:true },
            { path: '/tab/:mode/:main_init', component: TablePage , props:true },
            { path: '/tab/:mode/:main_init/:cols_init', component: TablePage , props:true },
        ] ;
        router = new VueRouter({routes}) ;
        app = new Vue ( { router } ) .$mount('#app') ;
    } ) ;
} ) ;
