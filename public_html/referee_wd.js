mw.loader.using(
  [
/*    'wikibase.ui.entityViewInit',
    'jquery.ui.draggable',
    'jquery.ui.droppable',*/
    'oojs',
    'oojs-ui-core',
    'oojs-ui-widgets'
  ],
  function onReady() {
    $(document).ready(refereeInit);
  },
  function onError(error) {
    console.error(error);
  },
);

if ( typeof referee_mode == 'undefined' ) referee_mode = 'auto' ; // 'auto' or 'manual'

function refereeInit () {

  var is_data_loaded = false ;
  var referee_api_url = 'https://tools.wmflabs.org/referee/api.php' ;
  var api = new mw.Api();

  var texts = (function texts() {
    var translations = {
        en: {
          saveFail: 'Unfortunately, saving failed',
          potentialReference: 'Potential reference from',
        },
      },
      chain = mw.language.getFallbackLanguageChain(),
      len = chain.length,
      ret = {},
      i = len - 1;

    while (i >= 0) {
      if (translations.hasOwnProperty(chain[i])) {
        $.extend(ret, translations[chain[i]]);
      }
      i -= 1;
    }
    return ret;
  }());


  // MAIN
  $('#p-tb').find('ul').append ( "<li><a id='t-referee' href='#'>Referee</a></li>" ) ;
  $('#t-referee').click ( function () {
    if ( is_data_loaded ) {
      $('div.referee-container').toggle() ;
    } else {
      run() ;
    }
  } ) ;
  if ( referee_mode == 'auto' ) {
    run() ;
  }


 function genericAPIaction(json, callback) {
    json.summary = '#referee';
    api
      .postWithEditToken(json)
      .done(callback)
      .fail(function err() {
        OO.ui.alert(texts.saveFail);
      });
  }

  function createReferenceCandidate ( statement , url ) {
    let sn = $('[id="'+statement.statement_id+'"]') ; // Statement node
    if ( $('.wikibase-edittoolbar-container').length == 0 ) {
      setTimeout ( function () { createReferenceCandidate ( statement , url ) } , 100 ) ;
      return ;
    }
    let json = JSON.parse ( statement.json ) ;
    let new_id = statement.statement_id + '-urlid' + url.id ;
    let h = "<div class='wikibase-statementview-references referee-container' id='"+new_id+"'>" ;
    h += "<div style='border-left:15px solid #6094DB;padding-left:3px;border-top:1px dotted #DDD;'>" ;
    h += "<div style='display:flex;flex-direction:row;'>"

    h += "<div style='display:flex;flex-direction:column;flex-grow:1;'>"
    h += "<div style='font-weight:bold;'>" + texts.potentialReference + " <a target='_blank' href='"+url.url+"'>"+url.server+"</a>:</div>" ;
    $.each ( json , function ( dummy , j ) {
      h += "<div style='flex-grow:1'>&hellip;" ;
      h += "<span>" + $('<span>').html(j.before).text() + "</span>" ;
      h += "<span style='margin-left:3px; margin-right:3px;'><b>" + $('<span>').html(j.match).text() + "</b></span>" ;
      h += "<span>" + $('<span>').html(j.before).text() + "</span>" ;
      h += "&hellip;</div>" ;
    } ) ;
    h += "</div>" ;

    h += "<div style='white-space:nowrap;margin-left:15px;margin-right:5px;' class='referee_actions_container'>" ;
    h += "<div><a href='#' class='referee_add_reference'>add reference</a></div>" ;
    h += "<div><a href='#' class='referee_remove_candidate'>remove candidate</a></div>" ;
    h += "</div>" ;

    h += "</div>" ;
    h += "</div>" ;
    h += "</div>" ;
    sn.append ( h ) ;
    $('[id="'+new_id+'"]').find('.referee_add_reference').click ( function () {
      console.log ( new_id ) ;
      $('[id="'+new_id+'"]').find('.referee_actions_container').html ( "<span style='color:green;font-size:14pt'>&#10003;</span>" ) ;
      let date = new Date() ;
      let this_time = '+' + date.toISOString().substr(0,10) + 'T00:00:00Z' ;
      let snaks = {
        P854:[{snaktype:'value',property:'P854',datavalue:{type:'string',value:url.url}}],
        P813:[{snaktype:'value',property:'P813',datavalue:{type:'time',value:{time:this_time,timezone:0,before:0,after:0,precision:11,calendarmodel:'http://www.wikidata.org/entity/Q1985727'}}}]
      } ;

      // Set reference in Wikidata
      genericAPIaction ( {
        action:'wbsetreference',
        statement:statement.statement_id,
        snaks:JSON.stringify(snaks),
        format:'json'
      } , function ( d ) {
        if ( d.success == 1 ) return ;
        console.log ( d ) ;
        OO.ui.alert(texts.saveFail);
      } ) ;

      $.getJSON ( referee_api_url+'?callback=?' , {
        action : 'set_candidate_status' ,
        statement_id : statement.id ,
        status : 'DONE' ,
        user : mw.user.getName()
      } , function ( d ) {
        if ( d.status == 'OK' ) return ;
        console.log(d) ;
      } ) ;

      return false ;
    } ) ;

    // Remove candidate reference in referee
    $('[id="'+new_id+'"]').find('.referee_remove_candidate').click ( function () {
      $('[id="'+new_id+'"]').remove() ;
      $.getJSON ( referee_api_url+'?callback=?' , {
        action : 'set_candidate_status' ,
        statement_id : statement.id ,
        status : 'REJECTED' ,
        user : mw.user.getName()
      } , function ( d ) {
        if ( d.status == 'OK' ) return ;
        console.log(d) ;
      } ) ;
      return false ;
    } ) ;
  }

  function run () {
    $.getJSON ( referee_api_url+'?callback=?' , {
      action:'get_statements_for_entity',
      entity:mw.config.get('wbEntityId')
    } , function ( d ) {
      if ( d.status != 'OK' ) {
        OO.ui.alert(d.status);
        return ;
      }
      is_data_loaded = true ;
      $.each ( d.statements , function ( dummy , statement ) {
        let url = d.urls[statement.url_id] ;
        createReferenceCandidate ( statement , url ) ;
      } ) ;
    } ) ; // TODO error handling
  }

}