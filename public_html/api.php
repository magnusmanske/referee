<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
header('Content-Type: application/json');

require_once ( '/data/project/referee/scripts/referee.php' ) ;

$referee = new Referee ;
$action = $referee->tfc->getRequest ( 'action' ) ;
$out = [ 'status' => 'OK' ] ;

if ( $action == 'get_statements_for_entity' ) {
	$entity = $referee->tfc->getRequest ( 'entity' ) ;
	$autorun = $referee->tfc->getRequest ( 'autorun' , 0 ) * 1 ;
	

	$url_ids = [] ;
	$out['statements'] = [] ;
	$out['urls'] = [] ;
	$sql = "SELECT * FROM statements WHERE `entity`='".$referee->dbt->real_escape_string($entity)."' AND `status`='TODO'" ;
	$result = $referee->getSQL ( $sql ) ;
	while($o = $result->fetch_object()) {
		$out['statements'][] = $o ;
		$url_ids[] = $o->url_id ;
	}

	if ( $autorun and count($out['statements']) == 0 ) { # Run parsing if not done before
		$referee->updateStatementReferenceCandidates ( $entity ) ;
		$sql = "SELECT * FROM statements WHERE `entity`='".$referee->dbt->real_escape_string($entity)."' AND `status`='TODO'" ;
		$result = $referee->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			$out['statements'][] = $o ;
			$url_ids[] = $o->url_id ;
		}
	}

	if ( count($url_ids) > 0 ) {
		$sql = "SELECT * FROM `urls` WHERE id IN (".implode(',',$url_ids).")" ;
		$result = $referee->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) {
			unset ( $o->contents ) ;
			$out['urls'][$o->id] = $o ;
		}
	}
} else if ( $action == 'set_candidate_status' ) {
	$statement_id = $referee->tfc->getRequest ( 'statement_id' ) * 1 ;
	$status = $referee->dbt->real_escape_string($referee->tfc->getRequest ( 'status' )) ;
	$user = $referee->dbt->real_escape_string($referee->tfc->getRequest ( 'user' )) ;

	$sql = "UPDATE `statements` SET `status`='{$status}',`username`='{$user}' WHERE id={$statement_id}" ;
	$referee->getSQL ( $sql ) ;
}

$callback = $referee->tfc->getRequest ( 'callback' ) ;
if ( $callback != '' ) print "{$callback}(" ;
print json_encode ( $out ) ;
if ( $callback != '' ) print ")" ;

?>