#!/usr/bin/php
<?php

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( '/data/project/referee/scripts/referee.php' ) ;

$referee = new Referee ;

if ( $argv[1] == 'rc' ) {
	$ts = '' ;
	while ( 1 ) {
		$referee->wil = new WikidataItemList ; # Save memory
		$hadthat = [] ;
		$sql = "SELECT rc_title,rc_timestamp FROM recentchanges WHERE rc_namespace=0 AND rc_timestamp>='{$ts}' ORDER BY rc_timestamp DESC LIMIT 10000" ; # Last edited items
		$result = $referee->tfc->getSQL ( $referee->dbwd , $sql ) ;
		while($o = $result->fetch_object()) {
			if ( isset($hadthat[$o->rc_title]) ) continue ;
			$hadthat[$o->rc_title] = 1 ;
			if ( $o->rc_timestamp > $ts ) $ts = $o->rc_timestamp ;
			$referee->updateStatementReferenceCandidates ( $o->rc_title ) ;
		}
		sleep(10) ;
	}
} else if ( $argv[1] == 'all' ) {

	$page_id = 0 ;
	$sql = "SELECT * FROM kv WHERE `key`='last_page_id'" ;
	$result = $referee->getSQL ( $sql ) ;
	if($o = $result->fetch_object()) $page_id = $o->value * 1 ;

	while ( 1 ) {
		$referee->wil = new WikidataItemList ; # Save memory
		$sql = "SELECT * FROM page WHERE page_id>{$page_id} AND page_namespace=0 ORDER BY page_id LIMIT 1" ;
		$result = $referee->tfc->getSQL ( $referee->dbwd , $sql ) ;
		if($o = $result->fetch_object()) {
			$page_id = $o->page_id ;
			$referee->updateStatementReferenceCandidates ( $o->page_title ) ;
			$sql = "UPDATE kv SET `value`='{$page_id}' WHERE `key`='last_page_id'" ;
#			print "{$sql}\n" ;
			$referee->getSQL ( $sql ) ;
		} else {
			exit(0); // All done
		}
	}

} else if ( preg_match ( '/^Q\d+$/' , $argv[1]) ) {
	$referee->updateStatementReferenceCandidates ( $argv[1] ) ;
}


?>