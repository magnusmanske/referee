<?php

require_once ( '/data/project/magnustools/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/magnustools/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class Referee {
	public $tfc ;
	public $dbt ;
	public $dbwd ;
	public $url_blacklist ;
	public $wil ;

	public $use_base64 = false ;
	public $no_refs_for_properties = [ 'P225' , 'P373' , 'P1472' , 'P1889' ] ;
	public $unspported_entity_markers = [
		[ 'P31' , 'Q13442814' ] , # Scholarly article
		[ 'P31' , 'Q16521' ] , # Taxon
		[ 'P31' , 'Q4167836' ] , # category
		[ 'P31' , 'Q4167410' ] , # disambiguation page
		[ 'P31' , 'Q5296' ] , # main page
	] ;

	function __construct () {
		$this->wil = new WikidataItemList ;
		$this->tfc = new ToolforgeCommon ( 'referee' ) ;
		$this->dbt = $this->tfc->openDBtool ( 'referee_p' ) ;
		$this->dbwd = $this->tfc->openDB ( 'wikidata' , 'wikidata' ) ;
	}

	public function getSQL ( $sql ) {
		return $this->tfc->getSQL ( $this->dbt , $sql ) ;
	}

	public function getTimestamp () {
		return time() ;
	}


	# URL related methods

	public function getServerFromURL ( $url ) {
		if ( !preg_match ( '|://(.+?)/|' , $url , $m ) ) return '' ;
		return $m[1] ;
	}

	public function isValidURL ( $url ) {
		if ( trim($url) == '' ) return false ;

		# Cache blacklist from database, if necessary
		if ( !isset($this->url_blacklist) ) {
			$this->url_blacklist = [] ;
			$sql = "SELECT * FROM url_pattern_blacklist" ;
			$result = $this->getSQL ( $sql ) ;
			while($o = $result->fetch_object()) {
				$this->url_blacklist[] = $o ;
			}
		}

		# Check URL against blacklist patterns
		foreach ( $this->url_blacklist AS $bl ) {
			$pattern = $bl->pattern ;
			$pattern = '|' . $pattern . '|i' ;
			if ( preg_match ( $pattern , $url ) ) return false ;
		}
		return true ;
	}

	public function getOrCreateURL ( $url ) {
		$sql = "SELECT * FROM `urls` WHERE `url`='" . $this->dbt->real_escape_string ( $url ) . "'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) return $o->id ;

		$server = $this->getServerFromURL ( $url ) ;
		$status = $this->isValidURL ( $url ) ? 'TODO' : 'INVALID' ;
		$sql = "INSERT IGNORE INTO `urls` (`url`,`server`,`timestamp`,`status`) VALUES (" .
			"'" . $this->dbt->real_escape_string ( $url ) . "'," .
			"'" . $this->dbt->real_escape_string ( $server ) . "'," .
			$this->getTimestamp() . "," .
			"'{$status}')" ;
		$this->getSQL ( $sql ) ;
		return $this->dbt->insert_id ;
	}

	public function loadContentsFromURL ( $url ) {
		$timeout = 60 ; # seconds
		$url = str_replace( "&amp;", "&", urldecode(trim($url)) ); # ???
		$url = str_replace ( ' ' , '%20' , $url ) ;
#		$cookie = tempnam ("/tmp", "CURLCOOKIE");
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		curl_setopt( $ch, CURLOPT_URL, $url );
#		curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_TIMEOUT, $timeout );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		$content = curl_exec( $ch );
		$response = curl_getinfo( $ch );
		curl_close ( $ch );
		if ( $response['http_code'] != 200 ) {
			$sql = "INSERT IGNORE INTO `bad_urls` (`url`,`code`,`json`) VALUES ('".$this->dbt->real_escape_string($url)."','".($response['http_code']*1)."','".$this->dbt->real_escape_string(json_encode($response))."')" ;
			$this->getSQL ( $sql ) ;
			return '' ;
		}

		if ( $response['content_type'] == '' ) return 'application/pdf' ; # Problems storing that

		return $content ;
	}

	public function getContentsFromURLID ( $url_id ) {
		$url_id *= 1 ;
		$sql = "SELECT * FROM `urls` WHERE `id`={$url_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) {}
		else return '' ; # Fallback; no such URL
		if ( $o->status == 'DONE' ) { # Using cache
			$ret = $o->contents ;
			if ( $o->content_format == 'BASE64' ) $ret = base64_decode ( $ret ) ;
			return $ret ;
		}
		if ( $o->status != 'TODO' ) return '' ; # Fallback; some failure

		# Load contents from URL
		$ret = $this->loadContentsFromURL ( $o->url ) ;
		$new_status = 'DONE' ;
		if ( $ret == '' ) $new_status = 'ERROR' ;

		# Store contents and status
		$contents = $ret ;
		$content_format = 'HTML' ;
		if ( $this->use_base64 ) {
			$contents = base64_encode ( $contents ) ;
			$content_format = 'BASE64' ;
		}
#print "!!" . strlen($contents) . "\n" ;
		if ( strlen($contents) > 5000000 ) { # No strings > ~5MB
			$new_status = 'ERROR' ;
			$contents = '' ;
			$ret = '' ;
		}
		$sql = "UPDATE `urls` SET contents='".$this->dbt->real_escape_string($contents)."',status='{$new_status}',`content_format`='{$content_format}' WHERE id={$o->id}" ;
		$this->getSQL ( $sql ) ;

		return $ret ;
	}


	# Statements

	public function getStatementsNeedingReferences ( $entity ) {
		$ret = [] ;
		$entity = trim ( strtoupper ( $entity ) ) ;
		$this->wil->loadItem ( $entity ) ;
		$i = $this->wil->getItem ( $entity ) ;
		if ( !isset($i) ) return $ret ;
		$j = $i->j ;
		if ( !isset($j->claims) ) return $ret ; # Has no claims
		foreach ( $j->claims AS $property => $claims ) {
			if ( in_array ( $property , $this->no_refs_for_properties ) ) continue ;
			foreach ( $claims AS $claim ) {
				if ( !isset($claim->mainsnak) or !isset($claim->mainsnak->datatype) ) continue ; # Proper statement
				if ( $claim->mainsnak->datatype == 'external-id' ) continue ; # No refs for external IDs
				if ( $claim->mainsnak->datatype == 'commonsMedia' ) continue ; # No refs for media
				$statement = (object) [
					'entity' => $entity ,
					'property' => $property ,
					'id' => $claim->id ,
					'claim' => $claim
				] ;
				$ret[] = $statement ;
			}
		}
		return $ret ;
	}

	public function HTML2text ( $html ) {
		$ret = $html ;
		$ret = preg_replace ( '/\n/' , " " , $ret ) ;
		$ret = preg_replace ( '/^.*<body.*?>/' , "" , $ret ) ;
		$ret = preg_replace ( '/<\/body>.*$/' , "" , $ret ) ;
		$ret = preg_replace ( '/<!--.*?-->/' , " " , $ret ) ;
		$ret = preg_replace ( '/<\/(p|div|br)>/i' , "\n" , $ret ) ;
		$ret = preg_replace ( '/<br\s*\/>/i' , "\n" , $ret ) ;
		$ret = preg_replace ( '/<.+?>/' , " " , $ret ) ;
		$ret = preg_replace ( '/[\r\t ]+/' , " " , $ret ) ;
		$ret = preg_replace ( '/ +\n/' , "\n" , $ret ) ;
		$ret = preg_replace ( '/\n +/' , "\n" , $ret ) ;
		$ret = preg_replace ( '/\n+/' , "\n" , $ret ) ;
		$ret = preg_replace ( '/ +/' , " " , $ret ) ;
		return $ret ;
	}

	public function guessPageLanguageFromText ( $text ) {
		$ret = 'en' ; # Default
		$candidates = [] ;
		$candidates['en'] = preg_match_all ( '/\b(he|she|it|is|was|the|a|an)\b/i' , $text ) ;
		$candidates['de'] = preg_match_all ( '/\b(er|sie|es|das|ein|eine|war|ist)\b/i' , $text ) ;
		$candidates['it'] = preg_match_all ( '/\b(è|una|della|la|nel|si|su|una|di)\b/i' , $text ) ;
		$candidates['fr'] = preg_match_all ( '/\b(est|un|une|et|la|il|a|de|par)\b/i' , $text ) ;
		$candidates['es'] = preg_match_all ( '/\b(el|es|un|de|a|la|es|conlas|dos)\b/i' , $text ) ;
		$best = 5 ; # Enforce default for incomprehensible text...
		foreach ( $candidates AS $language => $count ) {
			if ( $count <= $best ) continue ;
			$best = $count ;
			$ret = $language ;
		}
		return $ret ;
	}

	public function getCandidateURLsFromWikis ( $entity ) {
		$ret = [] ;
		$this->wil->loadItem ( $entity ) ;
		$i = $this->wil->getItem ( $entity ) ;
		if ( !isset($i) ) return $ret ;
		$sitelinks = $i->getSitelinks() ;
		foreach ( $sitelinks AS $wiki => $page ) {
			if ( preg_match ( '/:/' , $page ) ) continue ; # Poor man's namespace detection
			$server = $this->tfc->getWebserverForWiki ( $wiki ) ;
			$url = "https://{$server}/w/api.php?action=query&prop=extlinks&ellimit=500&elexpandurl=1&format=json&titles=" . urlencode(str_replace(' ','_',$page)) ;
			$j = json_decode ( file_get_contents ( $url ) ) ;
			foreach ( $j->query->pages AS $page_id => $page_info ) {
				if ( !isset($page_info->extlinks) ) continue ;
				foreach ( $page_info->extlinks AS $el ) {
					$url = $el->{'*'} ;
					if ( !preg_match ( '|^https{0,1}://|' , $url ) ) continue ;
					if ( preg_match ( '#\b(wikipedia|wikimedia|wik[a-z-]+)\.org/#' , $url ) ) continue ; # No Wikimedia "sources"

					$url_id = $this->getOrCreateURL ( $url ) ;
					if ( isset($ret[$url_id]) ) continue ;
					$contents = $this->getContentsFromURLID ( $url_id ) ;
					if ( $contents == '' ) continue ;
					$text = $this->HTML2text ( $contents ) ;
					$ret[$url_id] = (object) [
						'url' => $url ,
						'id' => $url_id ,
						'type' => 'wiki-exturl' ,
						'wiki' => $wiki ,
						'page' => $page ,
						'language' => $this->guessPageLanguageFromText ( $text ) ,
						'text' => $text
					] ;
				}
			}
		}
		return $ret ;
	}

	public function getCandidateURLs ( $entity ) {
		$ret = [] ;
		$this->wil->loadItem ( $entity ) ;
		$i = $this->wil->getItem ( $entity ) ;
		if ( !isset($i) ) return $ret ;
		$j = $i->j ;
		if ( !isset($j->claims) ) return $ret ; # Has claims
		$ret = $this->getCandidateURLsFromWikis ( $entity ) ;
		$official_websites = $i->getStrings ( 'P856' ) ;
		foreach ( $official_websites AS $ow ) $ret[] = $ow ;
		
		# TODO existing references

		foreach ( $j->claims AS $property => $claims ) {
			foreach ( $claims AS $claim ) {
				if ( !isset($claim->mainsnak) or !isset($claim->mainsnak->datatype) ) continue ; # Proper statement
				if ( $claim->mainsnak->datatype != 'external-id' ) continue ; # Skip other than external IDs
				if ( !isset($claim->mainsnak->datavalue) or !isset($claim->mainsnak->datavalue->value) ) continue ;
				$external_id = $claim->mainsnak->datavalue->value ;

				# Get property formatter URL
				$this->wil->loadItem ( $property ) ;
				$ip = $this->wil->getItem ( $property ) ;
				if ( !isset($ip) ) continue ;
				$formatter_urls = $ip->getStrings ( 'P1630' ) ;
				if ( count($formatter_urls) == 0 ) continue ;
				$url = str_replace ( '$1' , $external_id , $formatter_urls[0] ) ;
				$url_id = $this->getOrCreateURL ( $url ) ;
				if ( isset($ret[$url_id]) ) continue ;
				$contents = $this->getContentsFromURLID ( $url_id ) ;
				if ( $contents == '' ) continue ;
				$text = $this->HTML2text ( $contents ) ;
				$ret[$url_id] = (object) [
					'url' => $url ,
					'id' => $url_id ,
					'type' => 'external-id' ,
					'property' => $property ,
					'external_id' => $external_id ,
					'language' => $this->guessPageLanguageFromText ( $text ) ,
					'text' => $text
				] ;
			}
		}

		return $ret ;
	}

	public function getStatementSearchPatterns ( $statement , $language = 'en' ) {
		$ret = [] ;
		if ( !isset($statement->claim->mainsnak) or !isset($statement->claim->mainsnak->datavalue)  or !isset($statement->claim->mainsnak->datavalue->value) ) return $ret ;
		if ( in_array ( $statement->property , $this->no_refs_for_properties ) ) return $ret ;
		$dv = $statement->claim->mainsnak->datavalue ;
		$v = $dv->value ;

		if ( $dv->type == 'time' ) {
			if ( !preg_match ( '|^[+-]{0,1}0*(\d+)-(\d\d)-(\d\d)|' , $v->time , $m ) ) return $ret ;
			$year = $m[1] ;
			$month = str_pad($m[2],2,'0',STR_PAD_LEFT) ;
			$day = str_pad($m[3],2,'0',STR_PAD_LEFT) ;
			if ( $v->precision == 9 ) { # Year
				$ret[] = $year ;
			} else if ( $v->precision == 11 ) { # Day
				$fmt = new IntlDateFormatter($language.'_'.strtoupper($language), IntlDateFormatter::GREGORIAN, IntlDateFormatter::NONE);
				$ret[] = $fmt->format(mktime(null, null, null, $month*1, $day*1, $year*1));
				if ( preg_match ( '/^([A-Z][a-z]+)/' , $ret[0] , $m ) ) {
					$long_month = $m[1] ;
					$short_month = substr ( $long_month , 0 , 3 ) ;
					$ret[] = "{$day}. {$long_month} {$year}" ;
					$ret[] = "{$day} {$long_month} {$year}" ;
					$ret[] = "{$day}. {$short_month} {$year}" ;
					$ret[] = "{$day} {$short_month} {$year}" ;
					$ret[] = "{$long_month} {$day}, {$year}" ;
					$ret[] = "{$short_month} {$day}, {$year}" ;
				}
				$ret[] = "{$year}-{$month}-{$day}" ;
				$ret[] = "{$day}. {$month}. {$year}" ;
				$day *= 1 ;
				$month *= 1 ;
				$ret[] = "{$day}. {$month}. {$year}" ;
			}
		} else if ( $dv->type == 'string' ) {
			$ret[] = $v ;
		} else if ( $dv->type == 'monolingualtext' ) {
			$ret[] = $v->text ;
		} else if ( $dv->type == 'wikibase-entityid' ) {
			$this->wil->loadItem ( $v->id ) ;
			$vi = $this->wil->getItem ( $v->id ) ;
			if ( !isset($vi) ) return $ret ;

			$ret = $vi->getAliases ( $language ) ;
			$label = $vi->getLabel ( $language ) ;
			array_unshift ( $ret , $label ) ; # Make label first entry
			$ret2 = [] ;
			foreach ( $ret AS $r ) {
				$r = preg_quote ( trim ( $r ) ) ;
				if ( strlen($r) < 3 ) continue ;
				$ret2[] = $r ;
			}
			$ret = $ret2 ;

		} else if ( $dv->type == 'globecoordinate' ) {
			# Ignore
		} else if ( $dv->type == 'quantity' ) {
			# Ignore
		} else {
			print "{$dv->type}\n" ;
		}

		return $ret ;
	}

	public function doesStatementCandidateExist ( $statement_id , $url_id ) {
		$url_id *= 1 ;
		$sql = "SELECT * FROM `statements` WHERE `statement_id`='" . $this->dbt->real_escape_string($statement_id) . "' AND `url_id`={$url_id}" ;
		$result = $this->getSQL ( $sql ) ;
		if($o = $result->fetch_object()) return true ;
		return false ;
	}

	public function doesStatementHaveThisReference ( $statement , $url_candidate ) {
		$claim = $statement->claim ;
		if ( !isset($claim) or !isset($claim->references) ) return false ;
		foreach ( $claim->references AS $dummy => $reference ) {
			if ( isset($reference->snaks->P854) ) { # Reference URL
				foreach ( $reference->snaks->P854 AS $snak ) {
					if ( isset($snak->datavalue) and isset($snak->datavalue->value) and $snak->datavalue->value == $url_candidate->url ) return true ;
				}
			}
			if ( $url_candidate->type=='external-id' ) { # Reference prop=>value
				$ref_prop = $url_candidate->property ;
				if ( isset($reference->snaks->$ref_prop) ) {
					foreach ( $reference->snaks->$ref_prop AS $snak ) {
						if ( isset($snak->datavalue) and isset($snak->datavalue->value) and $snak->datavalue->value == $url_candidate->external_id ) return true ;
					}
				}
			}
		}
		return false ;
	}

	public function isSupportedEntity ( $entity ) {
		$this->wil->loadItem ( $entity ) ;
		$i = $this->wil->getItem ( $entity ) ;
		if ( !isset($i) ) return false ;

		foreach ( $this->unspported_entity_markers AS $entry ) {
			if ( $i->hasTarget ( $entry[0] , $entry[1] ) ) return false ;
		}
		return true ;
	}

	public function updateStatementReferenceCandidates ( $entity ) {
		$entity = trim ( strtoupper ( $entity ) ) ;
		if ( !$this->isSupportedEntity($entity) ) return ;
		$statements = $this->getStatementsNeedingReferences ( $entity ) ;
		if ( count($statements) == 0 ) return ;
		$url_candidates = $this->getCandidateURLs ( $entity ) ;
		if ( count($url_candidates) == 0 ) return ;

		# Get existing statement/URL candidates
		$has_statement = [] ;
		$sql = "SELECT * FROM `statements` WHERE `entity`='" . $this->dbt->real_escape_string($entity) . "'" ;
		$result = $this->getSQL ( $sql ) ;
		while($o = $result->fetch_object()) $has_statement["{$o->statement_id}|{$o->url_id}"] = 1 ;

		foreach ( $statements AS $statement ) {
			foreach ( $url_candidates AS $url_candidate ) {
				if ( isset($has_statement["{$statement->id}|{$url_candidate->id}"]) ) continue ;
				if ( $this->doesStatementHaveThisReference ( $statement , $url_candidate ) ) continue ;

				$parts = [] ;
				$patterns = $this->getStatementSearchPatterns ( $statement , $url_candidate->language ) ;
				foreach ( $patterns AS $pattern ) {
					if ( trim($pattern) == '' ) continue ;
					if ( !preg_match ( "|\b(.{0,60})\b({$pattern})\b(.{0,60})\b|is" , $url_candidate->text , $m ) ) continue ;
					$parts[] = (object) [
						'before' => $m[1] ,
						'match' => $m[2] ,
						'after' => $m[3]
					] ;
				}
				if ( count($parts) == 0 ) continue ;

				$json = json_encode($parts) ;
				if ( !isset($json) or $json == '' ) continue ; # TODO fixme
				$sql = "INSERT IGNORE INTO `statements` (`entity`,`property`,`statement_id`,`url_id`,`json`,`status`,`random`) VALUES (" ;
				$sql .= "'" . $this->dbt->real_escape_string ( $entity ) . "'," ;
				$sql .= "'" . $this->dbt->real_escape_string ( $statement->property ) . "'," ;
				$sql .= "'" . $this->dbt->real_escape_string ( $statement->id ) . "'," ;
				$sql .= $url_candidate->id . ',' ;
				$sql .= "'" . $this->dbt->real_escape_string ( $json ) . "'," ;
				$sql .= "'TODO',rand())" ;
				$this->getSQL ( $sql ) ;
			}
		}

	}

}

?>